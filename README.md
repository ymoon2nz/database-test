TEST Database
-------

Designed to demonstrate and test CI/CD and pipeline database deployment.

## Start with Docker

### Jenkins docker
```
docker run --name jenkins -p 8080:8080 -p 40000:50000 -d -v jenkins_home:/var/jenkins_home jenkins/jenkins

```

* Based on https://www.jenkins.io/doc/book/installing/docker/  
* User=juser
* Password=git5

Once Jenkins installation is completed and login is setup, then create new pipeline by click "New Item" and select Pipeline. 
Add this repository ``git@bitbucket.org:ymoon2nz/database-test.git`` (*may need to configure credentials) and "Build Configuration" with "by Jenkinsfile" at root(*default) path.

### Db2 docker
```
docker run -itd --name db2-dev --privileged=true -p 50000:50000 -e LICENSE=accept -e DB2INST1_PASSWORD=passDb2 ibmcom/db2:latest

# Check progress
docker logs db2-dev -f

# Once Db2 started
docker exec -it db2-dev bash
su - db2inst1
db2 create db DOCKER
db2 create db DEV
```
